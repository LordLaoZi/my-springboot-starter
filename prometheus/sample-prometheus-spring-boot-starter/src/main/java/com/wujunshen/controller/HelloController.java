package com.wujunshen.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/6/16 <br>
 * @time: 00:38 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@RestController
public class HelloController {

    @GetMapping(value = "/hello",
            produces = {MediaType.TEXT_PLAIN_VALUE})
    public String hello() {
        return "hello";
    }
}