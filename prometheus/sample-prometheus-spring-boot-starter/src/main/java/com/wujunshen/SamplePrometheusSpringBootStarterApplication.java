package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class SamplePrometheusSpringBootStarterApplication {

    public static void main(String[] args) {
        log.info("start SamplePrometheusSpringBootStarterApplication.....");
        SpringApplication.run(SamplePrometheusSpringBootStarterApplication.class, args);
        log.info("end SamplePrometheusSpringBootStarterApplication.....");
    }
}