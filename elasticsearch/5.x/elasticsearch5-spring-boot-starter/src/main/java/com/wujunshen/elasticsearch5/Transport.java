package com.wujunshen.elasticsearch5;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/7/2 <br>
 * @time: 13:26 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@Data
@ConfigurationProperties("es.transport")
public class Transport {
    //设置 true ，忽略连接节点集群名验证
    @Value("${es.transport.ignoreClusterName:true}")
    private String ignoreClusterName;
    //ping一个节点的响应时间 默认5秒
    @Value("${es.transport.pingTimeout:5s}")
    private String pingTimeout;
    //sample/ping 节点的时间间隔，默认是5s
    @Value("${es.transport.nodesSamplerInterval:5s}")
    private String nodesSamplerInterval;
}