package com.wujunshen.sample;

import com.wujunshen.redis.wrapper.MyRedisTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Author:frankwoo(吴峻申) <br>
 * Date:2017/9/15 <br>
 * Time:上午9:41 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleRedisSpringBootStarterApplicationTests.class)
@EnableAutoConfiguration
public class RedisConfigTest {
    @Resource
    private MyRedisTemplate myRedisTemplate;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetStringValue() {
        myRedisTemplate.setExpire("xx", "111", 60 * 60 * 2L);

        assertThat(myRedisTemplate.getBy("xx"), equalTo("111"));
    }
}