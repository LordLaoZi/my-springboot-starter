package com.wujunshen.sample.service;

import com.wujunshen.sample.entity.User;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/24 <br>
 * Time:  18:02 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public interface UserService {
    User getUserOfMasterDB(String userName, String password);

    User getUserOfSlaveDB(String userName, String password);

    void saveOfMasterDB(User user);

    void saveOfSlaveDB(User user);

    boolean existsOfMasterDB(String userName);

    boolean existsOfSlaveDB(String userName);
}