package com.wujunshen.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleMongodbSpringBootStarterApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleMongodbSpringBootStarterApplication.class, args);
    }
}