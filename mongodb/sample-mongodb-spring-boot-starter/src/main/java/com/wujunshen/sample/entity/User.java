package com.wujunshen.sample.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/23 <br>
 * Time:  14:08 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@Document(collection = "user")
public class User {
    @Id
    private String id;

    @Field("name")
    @Indexed(unique = true, background = true)
    private String userName;

    @Field("pass")
    private String password;
}